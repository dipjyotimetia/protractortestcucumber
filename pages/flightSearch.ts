import {$,browser,by,element} from 'protractor'

export class FlightSearch {
    public packageDeals: any;
    public origin: any;
    public destination: any;
    public departingDate: any;
    public destinationDate: any;

    constructor() {
        // this.packageDeals = element(by.id("tab-pa"));
        this.origin = element(by.id("jbBookerDepart"));
        this.destination = $("package-destination-hp-package");
        this.departingDate = $("package-departing-hp-package");
        // this.destinationDate = $("package-destination-hp-package");
    }
}