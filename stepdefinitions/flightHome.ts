import {browser} from "protractor";
import {FlightSearch} from '../pages/flightSearch'
import {defineSupportCode} from "cucumber";
import {async} from "q";

let chai = require('chai').use(require('chai-as-promised'));
let expect = chai.expect;

defineSupportCode(function ({Given, When, Then}) {
    let search: FlightSearch = new FlightSearch();

    Given(/^I am on expedia page$/, async () => {
        await expect(browser.getTitle()).to.eventually.equal('JetBlue | Airline Tickets, Flights, and Airfare');
    });
    When(/^I clicked on PackageDeals$/, async () =>{
        await search.origin.sendKeys("12/12/2017");
    });
    // Then(/^I click on origin$/,async()=>{
    //
    //  });

})